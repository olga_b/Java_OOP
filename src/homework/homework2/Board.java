package homework.homework2;

public class Board {
    private Shape[] shapes = new Shape[4];

    public Board() {

    }

    public void setShape(Shape shape) {
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] == null) {
                shapes[i] = shape;
                return;
            }
        }
    }

    public void deleteShape(Shape shape) {
        for (int i = 0; i < shapes.length; i++) {
            if (shapes[i] == shape) {
                shapes[i] = null;
                return;
            }
        }

    }

    public void printShapes() {
        double areaSum = 0;
        for (Shape figure : shapes) {
            if (figure != null) {
                System.out.println(figure.toString());
                areaSum += figure.getArea();
            }
        }
        System.out.println("Summarized area: " + areaSum);
    }
}
