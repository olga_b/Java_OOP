package homework.homework2;

public abstract class Shape {

    public abstract double getPerimeter();
    public abstract double getArea();

}
