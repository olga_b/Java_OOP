package homework.homework2;

public class Main {
    public static void main(String[] args) {

        Point a = new Point(1, 1);
        Point b = new Point(3, 4);
        Point c = new Point(5, 1);

        Triangle triangle = new Triangle(a, b, c);
        System.out.println("Perimeter of triangle: " + triangle.getPerimeter());
        System.out.println("Area of triangle: " + triangle.getArea());

        Point d = new Point(1, 4);
        Point e = new Point(5, 4);

        Rectangle rectangle = new Rectangle(a, d, e, c);
        System.out.println();
        System.out.println("Perimeter of rectangle: " + rectangle.getPerimeter());
        System.out.println("Area of rectangle: " + rectangle.getArea());

        Circle circle = new Circle(a, d);
        System.out.println();
        System.out.println("Perimeter of circle: " + circle.getPerimeter());
        System.out.println("Area of circle: " + circle.getArea());

        Board board = new Board();
        board.setShape(triangle);
        board.setShape(rectangle);
        board.setShape(circle);
        System.out.println();
        board.printShapes();
        board.deleteShape(rectangle);
        System.out.println();
        board.printShapes();



    }
}
