package homework.homework2;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        this.c = c;
    }

    @Override
    public double getPerimeter() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2)
                + Math.pow((b.getY() - c.getY()), 2));
        double ac = Math.sqrt(Math.pow((a.getX() - c.getX()), 2)
                + Math.pow((a.getY() - c.getY()), 2));

        return ab + bc + ac;
    }

    @Override
    public double getArea() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2)
                + Math.pow((b.getY() - c.getY()), 2));
        double ac = Math.sqrt(Math.pow((a.getX() - c.getX()), 2)
                + Math.pow((a.getY() - c.getY()), 2));
        double halfPerimeter = getPerimeter() / 2;
        double area = Math.sqrt(halfPerimeter*(halfPerimeter-ab)*(halfPerimeter-bc)*(halfPerimeter-ac));

        return area;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a.toString() +
                ", b=" + b.toString() +
                ", c=" + c.toString() +
                '}';
    }
}
