package homework.homework2;

public class Circle extends Shape {
    private Point a;
    private Point b;

    public Circle(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        return 2 * Math.PI * ab;
    }

    @Override
    public double getArea() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        return Math.PI * Math.pow(ab, 2);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "a=" + a.toString() +
                ", b=" + b.toString() +
                '}';
    }
}
