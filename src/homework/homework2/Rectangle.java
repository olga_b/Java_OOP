package homework.homework2;

public class Rectangle extends Shape{
    private Point a;
    private Point b;
    private Point c;
    private Point d;

    public Rectangle(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        this.a = a;
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        this.b = b;
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        this.c = c;
    }

    public Point getD() {
        return d;
    }

    public void setD(Point d) {
        this.d = d;
    }

    @Override
    public double getPerimeter() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2)
                + Math.pow((b.getY() - c.getY()), 2));
        double cd = Math.sqrt(Math.pow((c.getX() - d.getX()), 2)
                + Math.pow((c.getY() - d.getY()), 2));
        double ad = Math.sqrt(Math.pow((a.getX() - d.getX()), 2)
                + Math.pow((a.getY() - d.getY()), 2));

        return ab + bc + cd + ad;
    }

    @Override
    public double getArea() {
        double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2)
                + Math.pow((a.getY() - b.getY()), 2));
        double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2)
                + Math.pow((b.getY() - c.getY()), 2));

        return ab*bc;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "a=" + a.toString() +
                ", b=" + b.toString() +
                ", c=" + c.toString() +
                ", d=" + d.toString() +
                '}';
    }
}
