package homework.homework5;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DocFilter implements FileFilter {

    private Set<String> ext;

    public DocFilter(String [] ext) {
        this.ext = new HashSet<>(Arrays.asList(ext));
    }

    @Override
    public boolean accept(File file) {
        String fileName = file.getName();
        int index = fileName.lastIndexOf(".");
        String extension = fileName.substring(index + 1);

        return ext.contains(extension);
    }
}
