package homework.homework5;

import javax.swing.*;
import java.io.File;
import java.io.IOException;


public class Main {
    public static void main(String[] args) {

        String directoryPath = JOptionPane.showInputDialog("Enter path to directory");
        String outDirectoryPath = JOptionPane.showInputDialog("Enter path to output directory");


        File directory = new File(directoryPath);
        String [] ext = {"doc","txt"};
        DocFilter docFilter = new DocFilter(ext);
        if(directory.isDirectory()){

            for(File input:directory.listFiles(docFilter)){
                System.out.println(input.getName());
                try {
                    File output = new File(outDirectoryPath, input.getName());
                    FileCopy.copy(input,output);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        File file1 = new File("input.txt");
        File file2 = new File("input2.txt");
        try {
            CommonWords.copyCommonWords(file1,file2);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
