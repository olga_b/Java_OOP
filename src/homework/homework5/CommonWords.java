package homework.homework5;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CommonWords {

    public static File copyCommonWords(File file1, File file2) throws IOException {
        File result = new File("result.txt");
        Scanner scan1 = new Scanner(file1);
        Scanner scan2 = new Scanner(file2);
        scan1.useDelimiter(" ");
        scan2.useDelimiter(" ");
        Writer writer = new PrintWriter(result);

        Set<String> set = new HashSet<>();

        while(scan1.hasNext()) {
            set.add(scan1.next());
        }

        while(scan2.hasNext()){
            String word = scan2.next();
            if(set.contains(word)){
                writer.write(word+ " ");
            }
        }

        writer.flush();
        writer.close();

        return result;
    }
}
