package homework.homework5;

import java.io.*;

public class FileCopy {
    public static void copy(File from, File to) throws IOException{

        try(
            InputStream in = new FileInputStream(from);
            OutputStream out = new FileOutputStream(to)
        ){
            StreamCopy.copy(in,out);
        }


    }
}
