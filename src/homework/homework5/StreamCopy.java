package homework.homework5;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamCopy {

    public static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] bytes = new byte[10];
        int i;

        for (; (i = in.read(bytes)) != -1; ) {
            out.write(bytes, 0, i);
        }


    }
}
