package homework.collections.createAddPrintElements;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String[] stringList = new String[]{"firts", "second", "third", "fourth", "fifth", "sixth", "seventh","eighth", "ninth", "tenth"};
        Integer[] intList = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        Character[] charList = new Character[]{'q', 'w', 'e', 'r','t', 'y', '!', '@', '#', '$','%','^'};
        Double[]doubleList = new Double[]{1.23,2.34};
        createListRemoveSomeElementsPrint(stringList);
        createListRemoveSomeElementsPrint(intList);
        createListRemoveSomeElementsPrint(charList);
        createListRemoveSomeElementsPrint(doubleList);


    }

    public static <T> void createListRemoveSomeElementsPrint(T ... elements){
        int counter = 0;
        List<T> list = new ArrayList<>();
        if(elements == null){
            System.out.println("There are no elements");
            return;
        }
        //add elements to list
        for(T element:elements){
            if(element != null){
                list.add(element);
                counter++;
            }
            if(counter == 10){
                break;
            }
        }
        //remove two first and last element from list
        removeElement(list, 0);
        removeElement(list, 0);
        removeElement(list, list.size()-1);

        //print final list
        System.out.println(list);

    }
    private static <T> void removeElement(List<T> list, int index){
        if(index >= 0){
            list.remove(list.get(index));
        }
    }
}
