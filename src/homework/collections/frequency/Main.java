package homework.collections.frequency;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        //Set wordSet = new HashSet();
        Map <String, Integer> wordMap = new HashMap();
        try(Scanner scan = new Scanner(new FileReader("test/english"))) {
            scan.useDelimiter(" ");
            while(scan.hasNext()){
                String word = scan.next();
                for(int i=0;i<word.length();i++){
                    Integer temp = wordMap.get(word.charAt(i));
                    if(temp == null){
                        wordMap.put(String.valueOf(word.charAt(i)),1);
                    }else{
                        wordMap.put(String.valueOf(word.charAt(i)), temp++);
                    }

                }
            }
          Set<Map.Entry<String,Integer>> set = wordMap.entrySet();
            for(Map.Entry<String,Integer> entry:set){
                System.out.println(entry.getKey()+" : "+entry.getValue());
            }
        }catch (IOException e){
            System.out.println("File not found");
        }
    }
}
