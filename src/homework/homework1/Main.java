package homework.homework1;

public class Main {
    public static void main(String[] args) {

        //Task 1.
//      Описать класс «Cat» (в качестве образца можно взять домашнего питомца).
//      Наделить его свойствами и методами. Создать несколько экземпляров объектов этого
//      класса. Использовать эти объекты.
       Cat cat1 = new Cat("British", 1, "Whiskey");
        System.out.println(cat1);
        cat1.feed();
        cat1.isHungry();
        cat1.meow();

        Cat cat2 = new Cat("Maine Coon", 7, "Fox");
        System.out.println(cat2);

        //Task 2.
//      Описать класс «Triangle». В качестве свойств возьмите длины сторон
//      треугольника. Реализуйте метод, который будет возвращать площадь этого
//      треугольника. Создайте несколько объектов этого класса и протестируйте их.
        System.out.println();

        Triangle triangle1 = new Triangle(3, 4, 5);
        System.out.println(triangle1);
        System.out.println("S = " + triangle1.findS());

        Triangle triangle2 = new Triangle(5, 6, 7);
        System.out.println(triangle2);
        System.out.println("S = " + triangle2.findS());

        //Task 3.
//      Описать класс «Vector3d» (т.е. он должен описывать вектор в 3-х мерной, декартовой системе координат). В качестве свойств этого класса возьмите
//      координаты вектора. Для этого класса реализовать методы сложения, скалярного и
//      векторного произведения векторов. Создайте несколько объектов этого класса и
//      протестируйте их.
        System.out.println();
        Vector3d vector1 = new Vector3d(1f,0f,2f);
        Vector3d vector2 = new Vector3d(2.5f, 0.5f, 1.5f);
        System.out.println(vector1);
        System.out.println(vector2);
        System.out.println(vector1.findSum(vector1,vector2));
        System.out.println(vector1.findScalarSum(vector1,vector2));
        System.out.println(vector1.findVectorSum(vector1,vector2));

        //Task 4.
//      Опишите класс Phone (одним из свойств должен быть его номер). Также опишите
//      класс Network (сеть мобильного оператора).
//      Телефон должен иметь метод
//      регистрации в сети мобильного оператора. Также у телефона должен быть метод call(номер другого телефона), который переберет все телефоны, зарегистрированные в
//      сети. Если такой номер найден, то осуществить вызов, если нет - вывести сообщение
//      о неправильности набранного номера.
        System.out.println();

        Network network = new Network();
        Network network2 = new Network();

        Phone phone1 = new Phone("+380501234567");
        Phone phone2 = new Phone("+380501234568");
        Phone phone3 = new Phone("+380501234569");
        Phone phone4 = new Phone("+380631234501");
        phone1.registerPhone(network);
        phone2.registerPhone(network);
        phone4.registerPhone(network2);
        phone1.call("+380501234568");
        phone1.call("+380501234569");
        phone2.call("+380631234501");
        phone3.call("+380501234567");




    }
}
