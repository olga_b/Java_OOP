package homework.homework1;

import java.util.ArrayList;
import java.util.List;

public class Network {
    private List<String> numbers = new ArrayList<>();

    public void setNumber(String number) {
        this.numbers.add(number);
    }

    public boolean isNumber(String number){
        if(numbers.contains(number)){
            return true;
        }
        return false;
    }
}
