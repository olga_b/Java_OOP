package homework.homework1;

public class Vector3d {
    private float x;
    private float y;
    private float z;

    public Vector3d(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3d() {

    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public Vector3d findSum(Vector3d vector1, Vector3d vector2) {
        Vector3d sumVector = new Vector3d();

        sumVector.setX(vector1.getX() + vector2.getX());
        sumVector.setY(vector1.getY() + vector2.getY());
        sumVector.setZ(vector1.getZ() + vector2.getZ());

        return sumVector;
    }

    public float findScalarSum(Vector3d vector1, Vector3d vector2) {

        return (vector1.getX() * vector2.getX() +
                vector1.getY() * vector2.getY() +
                vector1.getZ() * vector2.getZ());
    }

    public Vector3d findVectorSum(Vector3d vector1, Vector3d vector2){
        Vector3d vectorSum = new Vector3d();

        vectorSum.setX(vector1.getY()*vector2.getZ() - vector1.getZ()*vector2.getY());
        vectorSum.setY(vector1.getZ()*vector2.getX() - vector1.getX()*vector2.getZ());
        vectorSum.setZ(vector1.getX()*vector2.getY() - vector1.getY()*vector2.getX());

        return vectorSum;
    }

    @Override
    public String toString() {
        return "Vector3d{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
