package homework.homework1;


public class Cat {
    private String breed;
    private int age;
    private String name;
    private boolean full;

    public Cat(String breed, int age, String name) {
        this.breed = breed;
        this.age = age;
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void feed(){
        if(!full){
            full = true;
        }
        System.out.println("Cat is fed");
    }

    public void isHungry(){
        full = false;
        System.out.println("Cat is hungry");
    }

    public void meow(){
        System.out.println("Meow-meow!");
    }

    @Override
    public String toString() {
        return "Cat{" +
                "breed='" + breed + '\'' +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
