package homework.homework1;

public class Phone {
    private String number;
    private Network network;

    public Phone(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void registerPhone(Network network) {
        this.network = network;
        network.setNumber(number);
    }

    public void call(String number) {
        if (network != null) {
            if (network.isNumber(number)) {
                System.out.println("We are calling to number: " + number);
                //incomingCall(this);

            } else {
                System.out.println("Number you are calling is incorrect!");
            }
        } else {
            System.out.println("Your number " + this.number + " isn't registered at any network");
        }

    }

    public void incomingCall(String number){
        if(network != null){
            System.out.println("Number "+ number +" is calling you.");
        }
        else{
            System.out.println("Your number isn't registered");
        }

    }
}