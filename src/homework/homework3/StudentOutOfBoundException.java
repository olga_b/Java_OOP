package homework.homework3;

public class StudentOutOfBoundException extends Exception {

    public String getMessage(){

        return "Group can contain only 10 students";
    }
}
