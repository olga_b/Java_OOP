package homework.homework3;

public class Student extends Person{
    private String faculty;
    private int course;

    public Student(String name, String surname, int age, String faculty, int course) {
        super(name, surname, age);
        this.faculty = faculty;
        this.course = course;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return super.toString() + ", "+
                "faculty='" + faculty + '\'' +
                ", course=" + course +
                '}' + '\n';
    }
}
