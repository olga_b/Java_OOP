package homework.homework3;

import java.util.*;

public class Group {

    private Student[] students = new Student[10];
    private int counter = 0;

    public Group() {

    }

    public void add(Student student) throws StudentOutOfBoundException {
        if(counter == 10) throw new StudentOutOfBoundException();
        if (students[counter] == null) {
            students[counter] = student;
            counter++;
        }

    }

    public void remove(Student student) {
        for (int i = 0; i < students.length; i++) {
            if (students[i].getSurname().equals(student.getSurname())) {
                students[i] = null;
                for(int j=i+1;j<students.length;j++){
                    students[j-1] = students[j];
                }
                counter--;
                break;
            }
        }
    }

    public List<Student> findBySurname(String surname) {
        List<Student> foundStudents = new ArrayList<>();
        for (int i = 0; i < students.length; i++) {
            if (students[i] != null && students[i].getSurname().equals(surname)) {
                foundStudents.add(students[i]);
            }
        }
        return foundStudents;
    }

    private Student[] getSortedGroup(){

        return Arrays.stream(students)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(Person::getSurname)).toArray(Student[]::new);

    }

    public Student[] getStudents() {
        return students.clone();
    }

    public Student[] sort(int i){
        Student [] tmp = getStudents();
        Comparator c;
        switch (i){
            case 0: c = new NameComparator();
            break;
            case 1: c = new SurnameComparator();
            break;
            case 2: c = new AgeComparator();
            break;
            case 3: c = new CourseComparator();
            break;
            default: c = new Comparator() {
                @Override
                public int compare(Object o, Object t1) {
                    return 0;
                }
            };
        }
        Arrays.sort(tmp, c);
        return tmp;
    }

    @Override
    public String toString() {
        return "Group{" +
                "students=" + Arrays.toString(getSortedGroup()) +
                '}';
    }



}


