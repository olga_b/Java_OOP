package homework.homework3;

import java.util.Comparator;

public class CourseComparator implements Comparator {
    @Override
    public int compare(Object o, Object t1) {
        if (o == null && t1 != null) {
            return -1;
        }
        if (o != null && t1 == null) {
            return 1;
        }
        if (o == null && t1 == null) {
            return 0;
        }
        Student student1 = (Student) o;
        Student student2 = (Student) t1;
        return student1.getCourse()-student2.getCourse();
    }
}
