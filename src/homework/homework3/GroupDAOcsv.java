package homework.homework3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GroupDAOcsv implements GroupDAO {

    File file;

    public GroupDAOcsv(File file) {
        this.file = file;
    }

    @Override
    public void save(Group group) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(file);
        Student[] students = group.getStudents();
        for (Student student : students) {
            if (student != null) {
                writer.write(student.getName() + "," + student.getSurname() + "," +
                        student.getAge() + "," + student.getFaculty() + "," + student.getCourse());
                writer.write(System.lineSeparator());
            }
        }

        writer.flush();
        writer.close();

    }

    @Override
    public Group read() throws FileNotFoundException {

        Scanner scan = new Scanner(file);
        Group group = new Group();

        List<String> strStudents = new ArrayList<>();

        while (scan.hasNextLine()) {
            strStudents.add(scan.nextLine());
        }

        for (String string : strStudents) {
            String[] params = string.split(",");
            Student student = new Student(params[0], params[1], Integer.valueOf(params[2]),
                    params[3], Integer.valueOf(params[4]));

            try {
                group.add(student);
            } catch (StudentOutOfBoundException e) {
                e.printStackTrace();
            }

        }


        return group;
    }
}
