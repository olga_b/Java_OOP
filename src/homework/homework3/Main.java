package homework.homework3;

import java.io.File;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Ivan","Ivanov", 20, "FIOT", 1);
        Student student2 = new Student("Peter","Petrov", 21, "FIOT", 1);
        Student student3 = new Student("Igor","Ivanov", 20, "FIOT", 1);
        Student student4 = new Student("Olena","Myronenko", 22, "FIOT", 2);
        Student student5 = new Student("Maria","Ivanova", 18, "FIOT", 1);
        Student student6 = new Student("Fedor","Sidorov", 21, "FIOT", 3);
        Student student7 = new Student("Nikolay","Faletskiy", 19, "FIOT", 1);
        Student student8 = new Student("Bohdan","Mateiko", 20, "FIOT", 2);
        Student student9 = new Student("Denis","Krasikov", 23, "FIOT", 4);
        Student student10 = new Student("Alex","Kubrak", 21, "FIOT", 1);
        Student student11 = new Student("Vasyl","Plotnikov", 20, "FIOT", 1);

        Group group = new Group();
        try {
            group.add(student1);
            group.add(student2);
            group.add(student3);
            group.add(student4);
            group.add(student5);
            group.add(student6);
            group.add(student7);
            group.add(student8);
            group.add(student9);
            group.add(student10);
            //group.add(student11);
        } catch(StudentOutOfBoundException ex){
            System.out.println(ex.getMessage());
        }

        System.out.println();
        System.out.println(group);

        group.remove(student3);

        System.out.println();
        System.out.println(group);

        try {
            group.add(student11);
        } catch (StudentOutOfBoundException e) {
            System.out.println(e.getMessage());
        }
        System.out.println();
        System.out.println(group);

        System.out.println("\nStudents with surname Ivanov:" + group.findBySurname("Ivanov"));
        System.out.println("\nStudents with surname Fedorov:" + group.findBySurname("Fedorov"));

        GroupDAOcsv groupDAOcsv = new GroupDAOcsv(new File("students.csv"));
        try {
            groupDAOcsv.save(group);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
