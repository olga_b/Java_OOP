package homework.homework3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

public class DaoMain {
    public static void main(String[] args) {
        GroupDAOcsv groupDAOcsv = new GroupDAOcsv(new File("students.csv"));
        Group group = null;

        try {
            group = groupDAOcsv.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(group);

        System.out.println(Arrays.toString(group.sort(2)));
    }
}
