package homework.homework3;

import java.io.IOException;

public interface GroupDAO {

    void save(Group group) throws IOException;
    Group read() throws IOException;
}
